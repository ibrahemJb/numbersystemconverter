package com.example.ibalout.numbersystemconverter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements RadioGroup.OnCheckedChangeListener {

    private EditText et_value;
    private TextView tv_converted_value;
    private RadioGroup rg_convert;
    private RadioButton rb_decimal;
    private RadioButton rb_binary;
    private RadioButton rb_octal;
    private RadioButton rb_hexadecimal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Initilize();
    }
    void Initilize(){

        et_value = findViewById(R.id.et_value);
        tv_converted_value = findViewById(R.id.tv_converted_value);
        rg_convert = findViewById(R.id.rg_convert);
        rb_decimal = findViewById(R.id.rb_decimal);
        rb_binary = findViewById(R.id.rb_binary);
        rb_octal = findViewById(R.id.rb_octal);
        rb_hexadecimal = findViewById(R.id.rb_hexadeciaml);

        et_value.addTextChangedListener(value_text_watcher);
        rg_convert.setOnCheckedChangeListener(this);

    }

    TextWatcher value_text_watcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            if(!editable.toString().equals("")) {
                int checkedId = rg_convert.getCheckedRadioButtonId();
                switch (checkedId) {
                    case R.id.rb_decimal: {
                        tv_converted_value.setText(rb_decimal.getText().toString() + " = " + editable.toString());
                        break;
                    }
                    case R.id.rb_binary: {
                        tv_converted_value.setText(rb_binary.getText().toString() + " = " + Long.toBinaryString(Double.doubleToRawLongBits(Double.parseDouble(editable.toString()))));
                        break;
                    }
                    case R.id.rb_octal:{
                        tv_converted_value.setText(rb_octal.getText().toString()+ " = " + );
                        break;
                    }
                }
            }
        }
    };


    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {
        if(radioGroup.getId() == R.id.rg_convert){

        }
    }
}
